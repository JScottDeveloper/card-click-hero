import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, TouchableOpacity, StyleSheet } from 'react-native';
import { withGlobalContext } from '../GlobalContext';


class LevelIdle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLevel: this.props.global.currentLevel,
      nextlevel: this.props.global.currentLevel + 1,
      nextlevelcost: 0,
      idle: this.props.global.idle,
      exp: this.props.global.exp,
    };
    //console.log(this.props.global.levels);

  }
  componentDidMount() {
    if (this.props.global.levels[this.state.currentLevel]) {
      this.setState({ nextlevelcost: this.props.global.levels[this.state.currentLevel].expcost });
    }

  }
  componentDidUpdate(prevProps) {
    if (this.props.currentLevel !== prevProps.currentLevel) {
      if (this.props.global.levels[this.props.currentLevel]) {
        this.setState({
          currentLevel: this.props.currentLevel,
          nextlevel: this.props.currentLevel + 1,
          nextlevelcost: this.props.global.levels[this.props.currentLevel].expcost
        });
      }

    }
  }
  render() {
    let idle = this.state.idle;
    
    return (
      <View style={{ flex: 3 }}>
        <ImageBackground
          style={{ flex: 1, paddingTop: 20, paddingBottom: 20, paddingLeft: 20, paddingRight: 20, }}
          imageStyle={{ resizeMode: 'stretch' }}
          source={require('../../assets/GUI/card.png')}
        >
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Image source={require('../../assets/GUI/gold_icon.png')} style={{ width: 25, height: 25, }} />
            <Text style={styles.normalText}>{idle.idlegp} / 5s</Text>
            <TouchableOpacity onPress={() => { this.props.global.upgradeGold() }} style={{ width: 25, height: 25, marginLeft: 10 }} >
              <Image source={require('../../assets/GUI/lvlup_btn.png')} style={{ width: 25, height: 25, }} />
            </TouchableOpacity>
            <Text style={styles.normalText}>Upgrade:</Text>
            <Image source={require('../../assets/GUI/gold_icon.png')} style={{ width: 20, height: 20, marginTop: 9 }} />
            <Text style={styles.normalText}>{idle.gpcost}</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Image source={require('../../assets/GUI/energy_icon.png')} style={{ width: 25, height: 25, }} />
            <Text style={styles.normalText}>{idle.idlexp} / 5s</Text>
            <TouchableOpacity onPress={() => { this.props.global.upgradeXp() }} style={{ width: 25, height: 25, marginLeft: 10 }} >
              <Image source={require('../../assets/GUI/lvlup_btn.png')} style={{ width: 25, height: 25, }} />
            </TouchableOpacity>
            <Text style={styles.normalText}>Upgrade:</Text>
            <Image source={require('../../assets/GUI/gold_icon.png')} style={{ width: 20, height: 20, marginTop: 9 }} />
            <Text style={styles.normalText}>{idle.xpcost} </Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ flex: 3, flexDirection: 'row' }}>
              <Image source={require('../../assets/GUI/gold_icon.png')} style={{ width: 25, height: 25, }} />
              <Text style={styles.normalText}>{idle.gpcollect}</Text>
              <Image source={require('../../assets/GUI/energy_icon.png')} style={{ width: 25, height: 25, }} />
              <Text style={styles.normalText}>{idle.xpcollect}</Text>
            </View>
            <View style={{ flex: 2, }}>
              <TouchableOpacity onPress={() => { this.props.global.collectRewards() }} style={{ flex: 1 }} >
                <ImageBackground
                  style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                  imageStyle={{ resizeMode: 'contain' }}
                  source={require('../../assets/GUI/cbtn_green.png')}
                >
                  <Text style={{ fontSize: 18, fontFamily: "Viga-Regular", fontWeight: 'bold', color: 'black', }}>Collect</Text>
                </ImageBackground>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 4, borderColor: 'black', borderWidth: 1 }}>
            <ImageBackground
              style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
              imageStyle={{ resizeMode: 'cover' }}
              source={require('../../assets/backgrounds/bgbs01.png')}
            >
              <Image resizeMode={'contain'} style={{ position: 'absolute', bottom: 20, right: 30, height: 80, width: 80, }} source={this.props.global.levels[this.state.currentLevel - 1].ecards[0].img} />
              <Image resizeMode={'contain'} style={{ position: 'absolute', bottom: 20, left: 30, height: 80, width: 80, transform: [{ rotateY: '180deg' }] }} source={(this.props.global.cardteam.length >= 1) ? this.props.global.cardteam[0].img : null} />
            </ImageBackground>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 3, }}>
              <TouchableOpacity onPress={() => { this.props.global.unlockNextLevel(this.state.currentLevel) }} style={{ flex: 1 }} >
                <ImageBackground
                  style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}
                  imageStyle={{ resizeMode: 'stretch' }}
                  source={require('../../assets/GUI/cbtn_green.png')}
                >
                  <Text style={{ fontSize: 16, fontFamily: "Viga-Regular", fontWeight: 'bold', color: 'black', }}>Unlock LVL {this.state.nextlevel}</Text>
                </ImageBackground>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, fontFamily: "Viga-Regular", fontWeight: 'bold', color: 'black', }}>Cost: {this.state.nextlevelcost}</Text>
              <Image source={require('../../assets/GUI/energy_icon.png')} style={{ width: 25, height: 25, }} />
            </View>
          </View>

        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  normalText: {
    fontSize: 16,
    marginTop: 5,
    fontFamily: "Viga-Regular",
    fontWeight: 'bold',
    color: 'black',
  }
});

export default withGlobalContext(LevelIdle);
