import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native';

import { withGlobalContext } from '../../GlobalContext';
import CardContainer from '../CardContainer';
import EnemyCardContainer from '../EnemyCardContainer';
import Combat from '../Combat';
import LevelButton from '../LevelButton';
import Banner from '../Banner';

// NOTES: Battle calculations
// hit = base * attack  ie. (1 * 10)
// dmg = hit - (hit / def) ie. (10 - (10/10))
// dmgresult = resistance * dmg  ie. (0.5 * 9) //if they resist
// else if weak it will be dmgresult = wk * dmg
// res will be elmnt sm med lg
// elmnt will be fire earth water


class Battle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playercardselect: [],
      enemycardselect: [],
      ecardi: null,
      pcardi: null,
      updateCombat: 0,
      ecards: null,
      pcards: null,
      updatecontainer: false
    };
    //console.log(this.state.ecards);
  };

  cardSelect = (item, i) => {
    //pick enemy card
    const eCards = this.state.ecards;
    const eCardI = Math.floor(Math.random() * eCards.length);
    var randomCard = eCards[eCardI];
    //find index of player selection in pCards
    const pCards = this.state.pcards

    let pCardI;
    for (var i = 0; i < pCards.length; i++) {
      if (pCards[i].puid == item.puid) {
        pCardI = i;
      }
    }


    this.setState({
      playercardselect: [item],
      enemycardselect: [randomCard],
      updateCombat: 1,
      ecardi: eCardI,
      pcardi: pCardI,

    })
  }

  enemyUpdate = () => {
    const eci = this.state.ecardi;
    const ec = this.state.ecards;
    //console.log(ec);
    ec.splice(eci, 1);
    //console.log(ec);
    this.setState({
      updateCombat: 0,
      ecards: ec,
      playercardselect: [],
      enemycardselect: [],

    })
  }

  playerUpdate = () => {
    const pci = this.state.pcardi;
    const pc = this.state.pcards;
    console.log(pci);
    console.log(pc);
    pc.splice(pci, 1);

    //console.log(pc);
    this.setState({
      updateCombat: 0,
      pcards: pc,
      playercardselect: [],
      enemycardselect: [],
      updatecontainer: !this.state.updatecontainer,
    })
  }

  componentDidMount() {
    const eteamcopy = JSON.parse(JSON.stringify(this.props.navigation.getParam('ecards', 'none')));
    const teamcopy = JSON.parse(JSON.stringify(this.props.global.cardteam));
    this.setState({ pcards: teamcopy, ecards: eteamcopy })
    if (this.props.global.cardteam.length <= 0) {
      alert("You havent selected a team yet! To select a team go to the 'Manage Cards' screen!");
    }
  }
  render() {
    const lvlName = this.props.navigation.getParam('name', 'UNK');
    const eCards = this.state.ecards;
    return (
      <ImageBackground source={require('../../../assets/backgrounds/bg09.png')} style={{ width: '100%', height: '100%', }}>
        <View style={styles.container}>
          <View style={styles.levelsHeader} >
            <Banner text={lvlName} />
          </View>
          <View style={{ flex: 2, alignSelf: 'stretch' }}>
            <EnemyCardContainer cards={eCards} />
          </View>
          <View style={{ flex: 4, alignSelf: 'stretch', flexDirection: 'row' }}>
            <Combat playercardselect={this.state.playercardselect}
              enemycardselect={this.state.enemycardselect}
              updateCombat={this.state.updateCombat}
              navigation={this.props.navigation}
              playerUpdate={this.playerUpdate}
              enemyUpdate={this.enemyUpdate}
              pcards={this.state.pcards}
              ecards={this.state.ecards}
            />
          </View>
          <View style={{ flex: 2, alignSelf: 'stretch' }}>
            <CardContainer cards={this.state.pcards} cardSelect={this.cardSelect} updated={this.state.updatecontainer} />
          </View>
        </View>
        <View style={styles.backContainer}>
          <LevelButton isUnlocked={true} onClick={() => { this.props.navigation.navigate('Main'); }} buttonText="Back" />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 8,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  levelsHeader: {
    height: 80,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    flexDirection: 'column',
    alignItems: 'center',
    width: 150,
    height: 220,
    borderRadius: 10,
    borderWidth: 5,
    borderColor: 'black',
    margin: 4,
    backgroundColor: '#99ff99',
  },
  backContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
});

export default withGlobalContext(Battle);
