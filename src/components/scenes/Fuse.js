import React, { Component } from 'react';
import { StyleSheet, ImageBackground, Image, Text, View, ScrollView } from 'react-native';
import SpriteSheet from 'rn-sprite-sheet';

import AllCards from '../../data/AllCards';
import { withGlobalContext } from '../../GlobalContext';
import FuseContainer from '../FuseContainer';
import CardGrid from '../CardGrid';
import LevelButton from '../LevelButton';
import Banner from '../Banner';
import BigCard from '../BigCard';
import Stats from '../Stats';


class Fuse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fuseCards: null,
      cardselect: null,
      cardsneeded: null,
      fuseUnlock: false,
      myCards: JSON.parse(JSON.stringify(this.props.global.cards)),
      successScreen: false,
      successCard: null,
    };
  }
  cardSelect = (item) => {
    let theCards = JSON.parse(JSON.stringify(AllCards));
    let myCards = this.state.myCards;
    let cardsNeeded = [];
    for (i = 0; i < item.fusedetails.length; i++) {
      let myQty = 0;
      for (y = 0; y < myCards.length; y++) {
        if (myCards[y].key == item.fusedetails[i].key && myCards[y].level == myCards[y].maxlevel) {
          myQty++;
        }
      }
      for (x = 0; x < item.fusedetails[i].qty; x++) {
        var temp = theCards.find((card) => { return card.key === item.fusedetails[i].key });
        if (myQty >= 1) {
          temp.haveIt = true;
          myQty--;
        } else { temp.haveIt = false; }

        cardsNeeded.push(JSON.parse(JSON.stringify(temp)));
      }
    }
    this.setState({
      cardselect: item,
      cardsneeded: cardsNeeded
    }, () => {
      let unlockcheck = 0;
      for (i = 0; i < this.state.cardsneeded.length; i++) {
        if (this.state.cardsneeded[i].haveIt == true) {
          unlockcheck++;
        }
        if (unlockcheck >= this.state.cardsneeded.length) {
          this.setState({ fuseUnlock: true });
        } else {
          this.setState({ fuseUnlock: false });
        }

      }
    });
  }
  FuseCards = (item) => {
    console.log('you just fused:');
    console.log(item.name);
    console.log('Removing:');
    let myCards = this.state.myCards;
    let puids = [];
    for (i = 0; i < item.fusedetails.length; i++) {
      let myQty = item.fusedetails[i].qty;
      for (x = 0; x < myCards.length; x++) {
        if (myCards[x].key == item.fusedetails[i].key && myCards[x].level == myCards[x].maxlevel) {
          if (myQty >= 1) {
            puids.push(myCards[x].puid);
            myQty--;
          }
        }
      }
      if (i == item.fusedetails.length - 1) {
        this.props.global.removeCard(puids)
        this.props.global.playFuse();
      }
    }

    this.props.global.setCards(item);
    //show success screen then reload component
    this.fuseplay('fuse');
    this.props.global.playFuse();
    this.setState({
      successScreen: true,
      successCard: item,
    })
  }
  fuseplay = type => {
    this.basicfuse.play({
      type,
      fps: Number(26),
      loop: false,
      resetAfterFinish: false,
      onFinish: () => {
        this.setState({
          cardselect: null,
          cardsneeded: null,
          myCards: JSON.parse(JSON.stringify(this.props.global.cards)),
        })
      }
    });
  };
  componentWillMount() {
    let theCards = JSON.parse(JSON.stringify(AllCards));
    const fuseCards = theCards.filter((card) => { return card.fuse === true });

    this.setState({ fuseCards: fuseCards });
  }
  render() {
    let cardselect;
    let thebutton;
    if (this.state.cardselect) {
      cardselect =
        <View style={{ height: 170 }}>
          <BigCard name={this.state.cardselect.name} element={this.state.cardselect.element} stars={this.state.cardselect.stars} level={this.state.cardselect.level} img={this.state.cardselect.img} flip={false} hp={this.state.cardselect.curhp} fullhp={this.state.cardselect.fullhp} attack={this.state.cardselect.attack} defense={this.state.cardselect.defense} />
        </View>;
      thebutton = <LevelButton buttonText={'FUSE'}
        onClick={() => { this.FuseCards(this.state.cardselect) }}
        isUnlocked={this.state.fuseUnlock} />;
    }
    return (
      <ImageBackground source={require('../../../assets/backgrounds/bg01.png')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.levelsHeader} >
          <Banner text="Fusion" />
        </View>
        <View style={{ flex: 1, flexDirection: 'column', marginTop: 5 }} >
          <View style={{ flex: 3, alignSelf: 'stretch', justifyContent: 'center', alignItems: 'center' }} >
            {cardselect}
            <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
              <SpriteSheet
                ref={ref => (this.basicfuse = ref)}
                source={require('../../../assets/battle/Fuse1.png')}
                columns={7}
                rows={2}
                // height={200} // set either, none, but not both
                // width={200}
                imageStyle={{ marginTop: -1 }}
                animations={{
                  fuse: [6, 7, 8, 9, 10, 9, 10, 9, 10, 9, 10, 11, 12, 13],
                }}
              />
            </View>
          </View>
          <View style={{ flex: 2, alignSelf: 'stretch', flexDirection: 'row' }} >
            <FuseContainer cards={this.state.cardsneeded} cardSelect={this.cardAdd} updated={this.state.updatecontainer} />
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {thebutton}
            </View>
          </View>
          <View style={{ flex: 3, alignSelf: 'stretch', }} >
            <CardGrid cards={this.state.fuseCards} cardSelect={this.cardSelect} updated={this.state.updated} />
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch', }} >

          </View>
        </View>
        <View style={styles.backContainer}>
          <LevelButton isUnlocked={true} onClick={() => { this.props.navigation.navigate('Main'); }} buttonText="Back" />
        </View>
        <Stats />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    flexDirection: 'column',
    alignItems: 'center',
    width: 150,
    height: 220,
    borderRadius: 10,
  },
  backContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  levelsHeader: {
    height: 80,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }

});
export default withGlobalContext(Fuse);
