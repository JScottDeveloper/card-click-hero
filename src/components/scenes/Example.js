import React from 'react';
import {
  SafeAreaView,
  View,
  Button,
  TextInput,
  Switch,
  Text,
  KeyboardAvoidingView
} from 'react-native';
import SpriteSheet from 'rn-sprite-sheet';
import { withGlobalContext } from '../../GlobalContext';

class Example extends React.Component {
  state = {
    loop: false,
    resetAfterFinish: false,
    fps: '16'
  };

  render() {
    const { fps, loop, resetAfterFinish } = this.state;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <SpriteSheet
              ref={ref => (this.attack = ref)}
              source={require('../../../assets/battle/basicShapes8.png')}
              columns={2}
              rows={5}
              // height={200} // set either, none, but not both
              // width={200}
              imageStyle={{ marginTop: -1 }}
              animations={{
                attack: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
              }}
            />
          </View>
          <View style={{ paddingVertical: 30, paddingHorizontal: 30 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <Button onPress={() => this.play('attack')} title="attack" />
              <Button onPress={this.stop} title="stop" />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, marginRight: 10 }}>FPS</Text>
              <TextInput
                style={{ flex: 1, borderBottomWidth: 1, fontSize: 16 }}
                value={fps}
                keyboardType="number-pad"
                onChangeText={fps => this.setState({ fps })}
              />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, marginRight: 10 }}>Loop</Text>
              <Switch value={loop} onValueChange={loop => this.setState({ loop })} />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, marginRight: 10 }}>Reset After Finish</Text>
              <Switch
                value={resetAfterFinish}
                onValueChange={val => this.setState({ resetAfterFinish: val })}
              />
            </View>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }

  play = type => {
    const { fps, loop, resetAfterFinish } = this.state;

    this.attack.play({
      type,
      fps: Number(fps),
      loop: loop,
      resetAfterFinish: resetAfterFinish,
      onFinish: () => console.log('hi')
    });
  };

  stop = () => {
    this.attack.stop(() => console.log('stopped'));
  };
}

export default withGlobalContext(Example);
