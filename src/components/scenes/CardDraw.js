import React, { Component } from 'react';
import { StyleSheet, Platform, ImageBackground, Image, Text, View, ScrollView, Dimensions } from 'react-native';
import firebase from 'react-native-firebase';
import SpriteSheet from 'rn-sprite-sheet';

import { withGlobalContext } from '../../GlobalContext';
import LevelButton from '../LevelButton';
import Banner from '../Banner';
import Stats from '../Stats';
import BigCard from '../BigCard';

var { height } = Dimensions.get('window');
var toprowh = height / 4;
var bottomrowh = height / 3;
class CardDraw extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawText: "",
      cardselect: null,
      timeRemaining: this.props.global.drawTimeRemaining,
    };

  }

  freeDraw = () => {
    if (this.props.global.drawTimeRemaining <= 0) {
      this.setState({ cardselect: null })
      this.props.global.basicFreeDraw();
      setTimeout(() => {
        if (this.props.global.drawTimeRemaining >= 1) {
          this.drawClock = setInterval(() => {
            this.setState({
              drawText: this.props.global.drawTimeRemaining,
              timeRemaining: this.props.global.drawTimeRemaining,
            });
            if (this.props.global.drawTimeRemaining <= 0) {
              clearInterval(this.drawClock);
            }
          }, 1000);
          this.props.global.playDraw();
          this.fuseplay('fuse');
          this.setState({ cardselect: this.props.global.newcard })
        } else {
          this.setState({
            drawText: "Free Draw"
          });
        }
      }, 200);

    }
  }
  goldDraw = () => {
    const gold = this.props.global.gold;
    if (gold >= 100) {
      this.setState({ cardselect: null })
      this.props.global.regBasicDraw();
      setTimeout(() => {
        this.props.global.playDraw();
        this.fuseplay('fuse');
        this.setState({ cardselect: this.props.global.newcard })
      }, 200)
    } else {
      alert('Not enough gold!');
    }
  }
  componentDidMount() {
    if (this.state.timeRemaining >= 1) {
      this.drawClock = setInterval(() => {
        this.setState({
          drawText: this.props.global.drawTimeRemaining
        });
        if (this.props.global.drawTimeRemaining <= 0) {
          clearInterval(this.drawClock);
        }
      }, 1000);
    } else {
      this.setState({
        drawText: "Free Draw"
      });
    }
  }
  fuseplay = type => {
    this.basicfuse.play({
      type,
      fps: Number(26),
      loop: false,
      resetAfterFinish: false,
      onFinish: () => { }
    });
  };
  componentWillUpdate(prevProps) {

  }
  componentWillUnmount() {
    clearInterval(this.drawClock);
    this.props.global.clearNewCard();
  }
  render() {
    let cardselect;
    if (this.state.cardselect) {
      cardselect =
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <View>
            <BigCard name={this.state.cardselect.name} element={this.state.cardselect.element} stars={this.state.cardselect.stars} level={this.state.cardselect.level} img={this.state.cardselect.img} flip={false} hp={this.state.cardselect.curhp} fullhp={this.state.cardselect.fullhp} attack={this.state.cardselect.attack} defense={this.state.cardselect.defense} />
          </View>
        </View>;
    }

    return (
      <ImageBackground source={require('../../../assets/backgrounds/bg10.png')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.levelsHeader} >
          <Banner text="Draw" />
        </View>
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }} >

          <View style={{ height: 170, alignSelf: 'stretch', flexDirection: 'row', justifyContent: 'center', }}>
            {cardselect}
            <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
              <SpriteSheet
                ref={ref => (this.basicfuse = ref)}
                source={require('../../../assets/battle/Fuse1.png')}
                columns={7}
                rows={2}
                // height={200} // set either, none, but not both
                // width={200}
                imageStyle={{ marginTop: -1 }}
                animations={{
                  fuse: [6, 7, 8, 9, 10, 9, 10, 9, 10, 9, 10, 11, 12, 13],
                }}
              />
            </View>
          </View>
          <View style={{ alignSelf: 'stretch', justifyContent: 'center', alignItems: 'center' }}>
            <LevelButton isUnlocked={true} onClick={() => { this.freeDraw(); }} buttonText={(this.state.drawText <= 0) ? "Free Draw" : this.state.drawText} />
          </View>
          <View style={{ alignSelf: 'stretch', justifyContent: 'center', alignItems: 'center' }}>
            <LevelButton isUnlocked={true} onClick={() => { this.goldDraw(); }} buttonText="100 Gold Draw" />
          </View>

        </View>

        <View style={styles.backContainer}>
          <LevelButton isUnlocked={true} onClick={() => { this.props.navigation.navigate('Main'); }} buttonText="Back" />
        </View>
        <Stats />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    flexDirection: 'column',
    alignItems: 'center',
    width: 150,
    height: 220,
    borderRadius: 10,
  },
  backContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  levelsHeader: {
    height: 80,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }

});
export default withGlobalContext(CardDraw);
