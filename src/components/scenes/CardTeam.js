import React, { Component } from 'react';
import { StyleSheet, Platform, ImageBackground, Image, Text, View, ScrollView, Dimensions } from 'react-native';
import firebase from 'react-native-firebase';
import SpriteSheet from 'rn-sprite-sheet';

import { withGlobalContext } from '../../GlobalContext';
import CardTeamContainer from '../CardTeamContainer';
import CardGrid from '../CardGrid';
import LevelButton from '../LevelButton';
import Banner from '../Banner';
import BigCard from '../BigCard';
import Stats from '../Stats';

var { height } = Dimensions.get('window');
var toprowh = height / 4;
var bottomrowh = height / 3;
class CardTeam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allcards: this.props.global.cards,
      thecteam: this.props.global.cardteam,
      updated: false,
      cardselect: null,
    };
    //this.setState({thecteam: this.props.global.cardteam})
  }

  cardRemove = (item) => {
    const cteam = this.props.global.cardteam;
    const allcards = this.props.global.cards;

    for (var i = 0; i < cteam.length; i++) {
      if (cteam[i].puid == item.puid) {
        cteam.splice(i, 1);
      }
    }

    allcards.push(item);

    this.props.global.updateCardTeam(cteam, allcards);
    this.setState({
      thecteam: this.props.global.cardteam,
      updated: !this.state.updated
    })
  }

  cardSelect = (item) => {
    this.setState({ cardselect: item })
  }

  cardAdd = (item) => {
    const cteam = this.props.global.cardteam;
    const allcards = this.props.global.cards;
    if (cteam.length <= 4) {
      this.props.global.playCard();
      cteam.push(item);
      for (var i = 0; i < allcards.length; i++) {
        if (allcards[i].puid == item.puid) {
          allcards.splice(i, 1);
        }
      }
      this.props.global.updateCardTeam(cteam, allcards);
      this.setState({
        thecteam: this.props.global.cardteam,
        updated: !this.state.updated,
        cardselect: null,
      })
    }
  }
  levelUp = (card) => {
    //console.log(card);
    var lvlcost = card.levelcost * card.level;
    if (lvlcost > this.props.global.exp) {
      alert('You dont have enought exp!');
    } else {
      if (card.level >= card.maxlevel) {
        alert('' + card.name + ' is already max level!');
      } else {
        var atk = Math.round(card.attack * card.attackmod);//to display to player
        var def = Math.round(card.defense * card.defensemod);//to display to player
        var hp = Math.round(card.fullhp * card.hpmod);//to display to player
        var lvlcard = {
          key: card.key,
          name: card.name,
          stars: card.stars,
          level: card.level + 1,
          levelcost: card.levelcost,
          maxlevel: card.maxlevel,
          attack: Math.round(card.attack * card.attackmod + card.attack),
          attackmod: card.attackmod,
          defense: Math.round(card.defense * card.defensemod + card.defense),
          defensemod: card.defensemod,
          base: Math.round(card.base * card.basemod + card.base),
          basemod: card.basemod,
          fullhp: Math.round(card.fullhp * card.hpmod + card.fullhp),
          hpmod: card.hpmod,
          curhp: Math.round(card.fullhp * card.hpmod + card.fullhp),
          draw: card.draw,
          weight: card.weight,
          build: card.build,
          buildetails: card.buildetails,
          element: card.element,
          weakness: card.weakness,
          resist: card.resist,
          img: card.img,
          power: card.power,
          puid: card.puid,
          atksprite: card.atksprite,
          spritecol: card.spritecol,
          spriterow: card.spriterow,
          spriteframes: card.spriteframes,
        }
        var swap = this.props.global.lvlCard(lvlcard);
        if (swap == true) {
          this.setState({
            allcards: this.props.global.cards,
            cardselect: lvlcard,
            updated: !this.state.updated,
          }, () => {
            this.levelplay('levelup');
            this.props.global.playLevelup();
            //also play sound for levelup
          })
        }
      }
    }
  }
  levelplay = type => {
    this.levelup.play({
      type,
      fps: Number(32),
      loop: false,
      resetAfterFinish: false,
      onFinish: () => {

      }
    });
  };
  componentDidMount() {
    if (this.props.global.cards <= 0) {
      alert("You dont have cards! What are you waiting for?! Go to the Draw screen and buy or get free cards!");
    }
  }
  render() {
    let cardselect;
    let lvlsection;
    if (this.state.cardselect) {
      //console.log(this.state.cardselect);
      if (this.state.cardselect.level <= this.state.cardselect.maxlevel) {
        lvlsection =
          <View style={{ flexDirection: 'row', alignSelf: 'stretch', alignItems: 'center' }}>
            <LevelButton isUnlocked={true} buttonText="LVL Up" onClick={() => { this.levelUp(this.state.cardselect) }} />
            <ImageBackground source={require('../../../assets/GUI/txt_bar.png')} style={{ width: 55, height: 20, marginLeft: 15, marginRight: 2, }}>
              <Text style={{ color: 'white', marginLeft: 15, fontFamily: "Viga-Regular", fontSize: 16 }}>{this.state.cardselect.levelcost * this.state.cardselect.level}</Text>
            </ImageBackground>
            <Image source={require('../../../assets/GUI/energy_icon.png')} style={{ width: 30, height: 30, position: 'absolute', top: 30, left: 80 }} />
          </View>
      }
      cardselect =
        <View style={{ flexDirection: 'row' }}>
          <View style={{ height: 170 }}>
            <BigCard name={this.state.cardselect.name} element={this.state.cardselect.element} stars={this.state.cardselect.stars} level={this.state.cardselect.level} img={this.state.cardselect.img} flip={false} hp={this.state.cardselect.curhp} fullhp={this.state.cardselect.fullhp} attack={this.state.cardselect.attack} defense={this.state.cardselect.defense} />
            <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
              <SpriteSheet
                ref={ref => (this.levelup = ref)}
                source={require('../../../assets/battle/basicShapes12.png')}
                columns={8}
                rows={2}
                // height={200} // set either, none, but not both
                // width={200}
                imageStyle={{ marginTop: -1 }}
                animations={{
                  levelup: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 10, 9, 10, 9, 10, 11, 12, 13, 14, 15],
                }}
              />
            </View>
          </View>

          <View>
            <LevelButton isUnlocked={true} buttonText="Add" onClick={() => { this.cardAdd(this.state.cardselect) }} />
            {lvlsection}
          </View>
        </View>;
    }
    return (
      <ImageBackground source={require('../../../assets/backgrounds/bg10.png')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.levelsHeader} >
          <Banner text="Cards" />
        </View>
        <View style={{ flex: 1, flexDirection: 'column', marginTop: 5 }} >
          <View style={{ flex: 1, alignSelf: 'stretch', marginTop: 10 }}>
            <CardTeamContainer cards={this.state.thecteam} cardRemove={this.cardRemove} updated={this.state.updated} />
          </View>
          <View style={{ height: bottomrowh - 50, alignSelf: 'stretch', flexDirection: 'row' }}>
            {cardselect}

          </View>
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
            <CardGrid cards={this.state.allcards} cardSelect={this.cardSelect} updated={this.state.updated} />
          </View>
        </View>

        <View style={styles.backContainer}>
          <LevelButton isUnlocked={true} onClick={() => { this.props.navigation.navigate('Main'); }} buttonText="Back" />
        </View>
        <Stats />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    flexDirection: 'column',
    alignItems: 'center',
    width: 150,
    height: 220,
    borderRadius: 10,
  },
  backContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  levelsHeader: {
    height: 80,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }

});
export default withGlobalContext(CardTeam);
