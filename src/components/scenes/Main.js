import React, { Component } from 'react';
import { StyleSheet, Image, ImageBackground, Text, View, ScrollView } from 'react-native';
import firebase from 'react-native-firebase';


import { withGlobalContext } from '../../GlobalContext';
import LevelButton from '../LevelButton';
import Banner from '../Banner';
import Stats from '../Stats';
import LevelIdle from '../LevelIdle';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      levels: [],
      cards: [],
      cardteam: [],
      selectedlvl: this.props.global.levels[0],
    };
  }

  componentDidMount() {
    firebase.auth().signInAnonymously()
      .then(() => {
        this.props.global.setAuthenticated();
      });
    //TODO: firebase get firestore and save to state if null then
    if (this.props.global.gamestart) {
      this.props.global.setLevels();
      this.props.global.selectLevel(this.state.selectedlvl);
    }

    this.props.global.setTimers();

    //TODO: send this data to firestore
    //check main theme and play
    this.props.global.checkTheme();


  }

  changeLevel = (level) => {
    this.setState({ selectedlvl: level })
  }

  levelList() {
    //console.log(this.state.cards);
    return this.props.global.levels.map((data, i) => {

      return (
        <View key={'lvlbtn' + i} style={styles.container}>
          <LevelButton buttonText={data.name} onClick={
            () => { this.props.navigation.navigate('Battle', { level: data.level, name: data.name, firstrewards: data.firstrewards, firstcheck: data.firstcheck, repeatrewards: data.repeatrewards, ecards: data.ecards, }) }
          } isUnlocked={data.idleunlocked} />
        </View>
      );
    });

  }
  render() {
    return (
      <ImageBackground source={require('../../../assets/backgrounds/bg10.png')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.levelsHeader} >
          <Banner text="Campaign" />
        </View>
        <View style={styles.levelListContainer} >
          <ImageBackground
            style={{ flex: 1, paddingTop: 15, paddingBottom: 15 }}
            imageStyle={{ resizeMode: 'stretch' }}
            source={require('../../../assets/GUI/card.png')}
          >
            <ScrollView style={{}}>
              {this.levelList()}
            </ScrollView>
          </ImageBackground>
          <LevelIdle currentLevel={this.props.global.currentLevel} />
        </View>

        <View style={styles.container}>
          <LevelButton buttonText={'Cards'} onClick={
            () => { this.props.navigation.navigate('CardTeam'); }
          } isUnlocked={true} />
          <LevelButton buttonText={'Draw'} onClick={
            () => { this.props.navigation.navigate('CardDraw'); }
          } isUnlocked={true} />
          <LevelButton buttonText={'Shop'} onClick={
            () => { this.props.navigation.navigate('None'); }
          } isUnlocked={true} />
          <LevelButton buttonText={'Fuse'} onClick={
            () => { this.props.navigation.navigate('Fuse'); }
          } isUnlocked={true} />
          <LevelButton buttonText={'???'} onClick={
            () => { this.props.navigation.navigate('None'); }
          } isUnlocked={true} />
        </View>
        <Stats />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  levelListContainer: {
    flex: 3,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  levelsHeader: {
    height: 80,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  statsContainer: {
    position: 'absolute',
    top: 20,
    right: 0,
  },
  normalText: {
    fontSize: 18,
    marginTop: 5,
    fontFamily: "Viga-Regular",
    fontWeight: 'bold',
    color: 'black',
  }
});
export default withGlobalContext(Main);
