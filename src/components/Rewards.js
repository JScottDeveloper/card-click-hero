import React, { Component } from 'react';
import { StyleSheet, Image, ImageBackground, Text, View, ScrollView } from 'react-native';
import { withGlobalContext } from '../GlobalContext';

class Stats extends Component {
  render() {
    return (
      <View style={styles.statsContainer}>
        <ImageBackground source={require('../../assets/GUI/txt_bar.png')} style={{ width: 95, height: 30, marginTop: 5, marginLeft: 15, marginRight: 2, paddingTop: 5 }}>
          <Text style={{ color: 'white', marginLeft: 30, fontFamily: "Viga-Regular", fontSize: 16 }}>{this.props.gold}</Text>
        </ImageBackground>
        <ImageBackground source={require('../../assets/GUI/txt_bar.png')} style={{ width: 95, height: 30, marginTop: 5, marginLeft: 15, marginRight: 2, paddingTop: 5 }}>
          <Text style={{ color: 'white', marginLeft: 30, fontFamily: "Viga-Regular", fontSize: 16 }}>{this.props.gems}</Text>
        </ImageBackground>
        <Image source={require('../../assets/GUI/gold_icon.png')} style={{ width: 40, height: 40, position: 'absolute', top: 0, left: 0 }} />
        <Image source={require('../../assets/GUI/gem_icon.png')} style={{ width: 40, height: 40, position: 'absolute', top: 35, left: 0 }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  statsContainer: {
    marginTop: 15,
  },
});

export default withGlobalContext(Stats);