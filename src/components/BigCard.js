import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, StyleSheet } from 'react-native';

import HealthBar from './HealthBar';

export default class BigCard extends Component {
  constructor(props) {
    super(props);
  };

  render() {
    let flip;
    if (this.props.flip) {
      flip = { transform: [{ rotateY: '180deg' }] };
    } else {
      flip = null;
    }
    let stars;
    let silver = <Image source={require('../../assets/GUI/star_silver.png')} resizeMode={'contain'} style={{ width: 20, height: '100%', marginTop: 0 }} />;
    let gold = <Image source={require('../../assets/GUI/star_gold.png')} resizeMode={'contain'} style={{ width: 20, height: '100%', marginTop: 0 }} />;
    switch (this.props.stars) {
      case 1:
        stars = <View style={{ height: 20, flexDirection: 'row' }}>{silver}</View>;
        break;
      case 2:
        stars = <View style={{ height: 20, flexDirection: 'row' }}>{silver}{silver}</View>;
        break;
      case 3:
        stars = <View style={{ height: 20, flexDirection: 'row' }}>{silver}{silver}{silver}</View>;
        break;
      case 4:
        stars = <View style={{ height: 20, flexDirection: 'row' }}>{gold}</View>;
        break;
      case 5:
        stars = <View style={{ height: 20, flexDirection: 'row' }}>{gold}{gold}</View>;
        break;
      case 6:
        stars = <View style={{ height: 20, flexDirection: 'row' }}>{gold}{gold}{gold}</View>;
        break;
    }
    let cardBg;
    switch (this.props.element) {
      case 1:
        cardBg = require('../../assets/GUI/card-normal.png');
        break;
      case 2:
        cardBg = require('../../assets/GUI/card-ground.png');
        break;
      case 3:
        cardBg = require('../../assets/GUI/card-electric.png');
        break;
      case 4:
        cardBg = require('../../assets/GUI/card-water.png');
        break;
      case 5:
        cardBg = require('../../assets/GUI/card-fire.png');
        break;
      case 6:
        cardBg = require('../../assets/GUI/card-forest.png');
        break;
    }
    return (
      <View style={styles.card}>
        <ImageBackground source={cardBg} imageStyle={{ width: '100%', height: '100%' }} style={styles.imgBG}>
          <ImageBackground source={require('../../assets/GUI/header.png')} imageStyle={{ resizeMode: 'stretch' }} style={{ width: '98%', height: 20, justifyContent: 'center', alignItems: 'center', marginTop: 1, marginLeft: 2 }}>
            <Text style={styles.cardTitle}>{this.props.name}</Text>
          </ImageBackground>
          <View style={{ width: '98%', height: 20, justifyContent: 'center', alignItems: 'center', marginLeft: 1 }}>
            {stars}
          </View>

          <Image resizeMode={'contain'} style={[styles.heroImg, flip]} source={this.props.img} />
          <HealthBar hp={this.props.hp} fullhp={this.props.fullhp} />
          <View style={{ flex: 1, alignSelf: 'stretch', marginBottom: 9, flexDirection: 'row', paddingRight: 5, paddingLeft: 5 }}>
            <ImageBackground source={require('../../assets/GUI/txt_bar.png')} imageStyle={{ resizeMode: 'stretch' }} style={{ flex: 1, marginLeft: -2, flexDirection: 'row', paddingTop: 5, paddingLeft: -2, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../../assets/GUI/power_icon.png')} resizeMode={'contain'} style={{ width: '30%', height: '100%', marginTop: 0 }} />
              <Text style={styles.cardStat}>{this.props.attack}</Text>
            </ImageBackground>
            <ImageBackground source={require('../../assets/GUI/txt_bar.png')} imageStyle={{ resizeMode: 'stretch' }} style={{ flex: 1, marginLeft: -2, flexDirection: 'row', paddingTop: 5, paddingLeft: -2, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../../assets/GUI/shield_icon.png')} resizeMode={'contain'} style={{ width: '30%', height: '100%', marginTop: 0 }} />
              <Text style={styles.cardStat}>{this.props.defense}</Text>
            </ImageBackground>
          </View>
          <ImageBackground imageStyle={{ resizeMode: 'stretch' }} source={require('../../assets/GUI/txt_bar.png')} style={{ width: 25, height: 25, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 20, right: 3 }}>
            <Text style={{ color: 'white', fontFamily: "Viga-Regular", fontSize: 16 }}>{this.props.level}</Text>
          </ImageBackground>
        </ImageBackground>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  heroImg: {
    height: '40%',
    width: '90%',
    marginTop: 1
  },
  cardTitle: {
    fontWeight: 'bold',
    fontSize: 17,
    color: 'black',
    fontFamily: "Viga-Regular",
  },
  imgBG: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',

    opacity: 1,
  },
  card: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    width: 130,
    height: 220,
    borderRadius: 10,
  },
  cardStat: {
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white',
    fontFamily: "Viga-Regular",
    alignSelf: 'stretch'
  },
});
