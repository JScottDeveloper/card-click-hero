import React, { Component } from 'react';
import { FlatList, Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native';

import SmallCard from './SmallCard';

export default class CardTeamContainer extends Component {
  constructor(props) {
    super(props);
  };
  cardRemove(item) {
    this.props.cardRemove(item);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <FlatList style={styles.flatlist} horizontal={true} data={this.props.cards}
            renderItem={({ item }) => {
              return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => this.cardRemove(item)}>
                    <SmallCard name={item.name} element={item.element} stars={item.stars} level={item.level} img={item.img} flip={false} hp={item.curhp} fullhp={item.fullhp} attack={item.attack} defense={item.defense} />
                  </TouchableOpacity>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.props.updated}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  innerContainer: {
    height: 150
  },
  flatlist: {
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  card: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    width: 110,
    borderRadius: 10,
  }
});
