import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
// This is my custom button Component
// When using this component you must pass
// onPress prop, buttonText prop

const UIButton = (props) => {
  return (
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={props.onClick}
      >
        <Text
          style={styles.buttonText}
        >{props.buttonText}</Text>
      </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginBottom: 5,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#99ff99',
    borderWidth: 1,
    borderColor: 'black',

  },
  buttonText: {
    fontFamily: "Viga-Regular",
    fontSize: 18,
    textAlign: 'center',
    color: 'black',
    margin: 15,
  }
});

export default UIButton;
