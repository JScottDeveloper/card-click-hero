import React, { Component } from 'react';
import { FlatList, Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native';

import GridCard from './GridCard';

export default class CardContainer extends Component {
  constructor(props) {
    super(props);
  };
  cardSelect(item) {

  }
  render() {
    let thetext;
    if (this.props.cards) {
      thetext = <Text style={{ color: 'white', fontFamily: "Viga-Regular", fontSize: 12 }}>Cards Needed To Fuse:</Text>;
    }
    return (
      <View style={styles.container}>
        {thetext}
        <FlatList style={styles.flatlist} horizontal={true} data={this.props.cards}
          renderItem={({ item }) => {
            return (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 100, }}>
                <TouchableOpacity onPress={() => this.cardSelect(item)}>
                  <GridCard name={item.name} element={item.element} stars={item.stars} level={item.maxlevel} img={item.img} flip={false} />
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.props.updated}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flatlist: {
    backgroundColor: 'rgba(0,0,0,0.5)',
  },

});
