import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, Button } from 'react-native';
import LevelButton from './LevelButton';
import Rewards from './Rewards';
import { withGlobalContext } from '../GlobalContext';


class LevelSplash extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {
    if (this.props.text == "VICTORY!") {
      this.props.global.playVictory();
    }
  }

  render() {

    return (
      <View style={styles.container}>
        <View style={styles.panel}>
          <ImageBackground source={require('../../assets/GUI/win-w-head.png')} imageStyle={{ resizeMode: 'contain' }} style={{ width: '100%', height: '100%', flexDirection: 'column', alignItems: 'center', paddingTop: 18, }}>
            <Text style={styles.header}>{this.props.text}</Text>
            <Rewards gold={this.props.gold} gems={this.props.gems} />
            <LevelButton
              onClick={() => { this.props.navigation.navigate('Main', { example: 'blah' }) }}
              buttonText="OK"
              isUnlocked={true}
            />
          </ImageBackground>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  panel: {
    width: 300,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  header: {
    fontSize: 24,
    fontFamily: "Viga-Regular",
    fontWeight: 'bold',
    color: 'black',
  },
  rwdheader: {
    fontSize: 24,
    marginTop: 5,
    fontFamily: "Viga-Regular",
    fontWeight: 'bold',
    color: 'black',
  },
  rewards: {
    fontSize: 18,
    fontFamily: "Viga-Regular",
    color: 'black'
  }

});

export default withGlobalContext(LevelSplash);