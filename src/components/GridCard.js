import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, StyleSheet } from 'react-native';

export default class GridCard extends Component {
  constructor(props) {
    super(props);
  };

  render() {
    let stars;
    let silver = <Image source={require('../../assets/GUI/star_silver.png')} resizeMode={'contain'} style={{ width: 20, height: '100%', marginTop: 0 }} />;
    let gold = <Image source={require('../../assets/GUI/star_gold.png')} resizeMode={'contain'} style={{ width: 20, height: '100%', marginTop: 0 }} />;
    switch(this.props.stars){
      case 1:
        stars = <View style={{height: 20, flexDirection: 'row'}}>{silver}</View>;
        break;
      case 2:
        stars = <View style={{height: 20, flexDirection: 'row'}}>{silver}{silver}</View>;
        break;
      case 3:
        stars = <View style={{height: 20, flexDirection: 'row'}}>{silver}{silver}{silver}</View>;
        break;
      case 4:
        stars = <View style={{height: 20, flexDirection: 'row'}}>{gold}</View>;
        break;
      case 5:
        stars = <View style={{height: 20, flexDirection: 'row'}}>{gold}{gold}</View>;
        break;
      case 6:
        stars = <View style={{height: 20, flexDirection: 'row'}}>{gold}{gold}{gold}</View>;
        break;
    }
    let cardBg;
    switch (this.props.element) {
      case 1:
        cardBg = require('../../assets/GUI/card-normal.png');
        break;
      case 2:
        cardBg = require('../../assets/GUI/card-ground.png');
        break;
      case 3:
        cardBg = require('../../assets/GUI/card-electric.png');
        break;
      case 4:
        cardBg = require('../../assets/GUI/card-water.png');
        break;
      case 5:
        cardBg = require('../../assets/GUI/card-fire.png');
        break;
      case 6:
        cardBg = require('../../assets/GUI/card-forest.png');
        break;
    }
    return (
      <View style={styles.card}>
        <ImageBackground source={cardBg} imageStyle={{ resizeMode: 'stretch' }} style={styles.imgBG}>
          <Image resizeMode={'contain'} style={styles.heroImg} source={this.props.img} />

          <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 1, position: 'absolute', top: 1, left: 1 }}>
            {stars}
          </View>

          <ImageBackground imageStyle={{ resizeMode: 'stretch' }} source={require('../../assets/GUI/txt_bar.png')} style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 1, right: 1 }}>
            <Text style={{ color: 'white', fontFamily: "Viga-Regular", fontSize: 12 }}>{this.props.level}</Text>
          </ImageBackground>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  heroImg: {
    height: '100%',
    width: '100%',
    marginTop: 0
  },
  imgBG: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    opacity: 1,
    padding: 1,
  },
  card: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    width: 100,
    height: 100,
  },
});
