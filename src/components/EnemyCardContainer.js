import React, { Component } from 'react';
import { FlatList, Text, View, StyleSheet, ImageBackground, Image } from 'react-native';

import SmallCard from './SmallCard';

export default class CardContainer extends Component {
  constructor(props) {
    super(props);
  };
  render() {
    let uid = new Date().getUTCMilliseconds();
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <FlatList style={styles.flatlist} horizontal={true} data={this.props.cards}
            renderItem={({ item, i }) => {
              return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <SmallCard name={item.name} element={item.element} stars={item.stars} level={item.level} img={item.img} flip={true} hp={item.curhp} fullhp={item.fullhp} attack={item.attack} defense={item.defense} />
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  innerContainer: {
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  flatlist: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignSelf: 'stretch',
  },
  card: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: 110,
    borderRadius: 10,

  }
});
