const images = {
  image1: require('../../assets/hero/001.png'),
  image2: require('../../assets/hero/002.png'),
  image3: require('../../assets/hero/003.png'),
  image4: require('../../assets/hero/004.png'),
  image5: require('../../assets/hero/005.png'),
  image6: require('../../assets/hero/006.png'),
  image7: require('../../assets/hero/007.png'),
  image8: require('../../assets/hero/008.png'),
  image9: require('../../assets/hero/009.png'),
  image10: require('../../assets/hero/010.png'),
  image11: require('../../assets/hero/011.png'),
  image12: require('../../assets/hero/012.png'),
  image13: require('../../assets/hero/013.png'),
  image14: require('../../assets/hero/014.png'),
  image15: require('../../assets/hero/015.png'),
  image16: require('../../assets/hero/016.png'),
  image17: require('../../assets/hero/017.png'),
  image18: require('../../assets/hero/018.png'),
  image19: require('../../assets/hero/019.png'),
  image20: require('../../assets/hero/020.png'),
};

export default images;
