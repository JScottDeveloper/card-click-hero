import React, { Component } from 'react';
import { StyleSheet, Image, ImageBackground, Text, View, ScrollView } from 'react-native';
import { withGlobalContext } from '../GlobalContext';

class Stats extends Component {
  render() {
    return (
      <View style={styles.statsContainer}>
        <ImageBackground source={require('../../assets/GUI/txt_bar.png')} style={{ width: 95, height: 22, marginLeft: 15, marginRight: 2, marginTop: 7 }}>
          <Text style={{ color: 'white', marginLeft: 30, fontFamily: "Viga-Regular", fontSize: 16 }}>{this.props.global.gold}</Text>
        </ImageBackground>
        <ImageBackground source={require('../../assets/GUI/txt_bar.png')} style={{ width: 95, height: 22, marginLeft: 15, marginRight: 2, marginTop: 7 }}>
          <Text style={{ color: 'white', marginLeft: 30, fontFamily: "Viga-Regular", fontSize: 16 }}>{this.props.global.exp}</Text>
        </ImageBackground>
        <ImageBackground source={require('../../assets/GUI/txt_bar.png')} style={{ width: 95, height: 22, marginLeft: 15, marginRight: 2, marginTop: 7 }}>
          <Text style={{ color: 'white', marginLeft: 30, fontFamily: "Viga-Regular", fontSize: 16 }}>{this.props.global.gems}</Text>
        </ImageBackground>
        <Image source={require('../../assets/GUI/gold_icon.png')} style={{ width: 30, height: 30, position: 'absolute', top: 1, left: 0 }} />
        <Image source={require('../../assets/GUI/energy_icon.png')} style={{ width: 30, height: 30, position: 'absolute', top: 30, left: 0 }} />
        <Image source={require('../../assets/GUI/gem_icon.png')} style={{ width: 30, height: 30, position: 'absolute', top: 62, left: 0 }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  statsContainer: {
    position: 'absolute',
    top: 10,
    right: 0,
  },
});

export default withGlobalContext(Stats);
