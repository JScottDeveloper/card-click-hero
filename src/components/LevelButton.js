import React from 'react';
import { StyleSheet, Text, TouchableOpacity, ImageBackground } from 'react-native';
// This is my custom button Component
// When using this component you must pass
// onPress prop, buttonText prop

const LevelButton = (props) => {
  const isUnlocked = props.isUnlocked;

  if (isUnlocked) {
    return (
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={props.onClick}
      >
        <ImageBackground source={require('../../assets/GUI/lvl_btn.png')} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <Text
            style={styles.buttonText}
          >{props.buttonText}</Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        style={styles.buttonContainer}
      >
        <ImageBackground source={require('../../assets/GUI/lock_btn.png')} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>

        </ImageBackground>
      </TouchableOpacity>
    );
  }

};

const styles = StyleSheet.create({
  buttonContainer: {
    marginBottom: 5,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 80,
    width: 80,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: "Viga-Regular",
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
    paddingLeft: 10,
    paddingRight: 10,
  }
});

export default LevelButton;
