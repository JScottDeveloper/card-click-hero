import React, { Component } from "react";
import { View, Animated, Text, ImageBackground, Image } from "react-native";

class HealthBar extends Component {
  state = {

  };

  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.animation = new Animated.Value(this.props.hp);
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.hp !== this.props.hp) {
      Animated.timing(this.animation, {
        duration: 1500,
        toValue: this.props.hp
      }).start();
    }
  }

  render() {
    let maxRange;
    if (this.props.fullhp >= 1) {
      maxRange = this.props.fullhp;
    } else {
      maxRange = 100;
    }
    const widthInterpolated = this.animation.interpolate({
      inputRange: [0, this.props.fullhp],
      outputRange: ["0%", "80%"],
      extrapolate: 'clamp'
    })
    return (
      <ImageBackground source={require('../../assets/GUI/bar_container.png')} imageStyle={{ resizeMode: 'stretch' }} style={{ width: '98%', height: 20, }}>
        <View style={{ flex: 1 }}>
          <Animated.View style={{ position: 'absolute', left: 13, top: 7, bottom: 7, width: widthInterpolated, backgroundColor: '#c42525' }} >
          </Animated.View>
          <View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }} >
            <Text style={styles.cardTitle}>{this.props.hp}</Text>
          </View>
        </View>

      </ImageBackground>
    );
  }
}

const styles = {
  cardTitle: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'black',
    fontFamily: "Viga-Regular",
    zIndex: 1,
  },
};

export default HealthBar;
