import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
// This is my custom button Component
// When using this component you must pass
// onPress prop, buttonText prop

const Banner = (props) => {
  return (
    <View style={styles.container} >
      <ImageBackground source={require('../../assets/GUI/cbtn_grey.png')} imageStyle={{ resizeMode: 'cover' }} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
        <Text style={styles.text} >{props.text}</Text>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 170,
  },
  text: {
    fontSize: 28,
    fontFamily: "Viga-Regular",
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
  }
});

export default Banner;
