import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native';
import SpriteSheet from 'rn-sprite-sheet';

import { withGlobalContext } from '../GlobalContext';
import LevelSplash from './LevelSplash';
import BigCard from './BigCard';

class Combat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pcardname: '',
      ecardname: '',
      ecardstars: 0,
      ecardlevel: 0,
      pcardhp: 0,
      pcardhptot: 0,
      pcardatk: 0,
      pcarddef: 0,
      pcardbase: 0,
      ecardhp: 0,
      ecardhptot: 0,
      ecardatk: 0,
      ecarddef: 0,
      ecardbase: 0,
      pturn: true,
      popacity: 1,
      eopacity: 1,
      endlevel: 0,
      gold: 0,
      gems: 0,
    };
  }
  componentDidUpdate(prevProps) {
    //console.log(this.props.updateCombat)
    if (this.props.updateCombat != prevProps.updateCombat && this.props.updateCombat == 1) {
      this.setState({
        pcardname: this.props.playercardselect[0].name,
        ecardname: this.props.enemycardselect[0].name,
        ecardstars: this.props.enemycardselect[0].stars,
        ecardlevel: this.props.enemycardselect[0].level,
        pcardstars: this.props.playercardselect[0].stars,
        pcardlevel: this.props.playercardselect[0].level,
        pcardhp: this.props.playercardselect[0].curhp,
        pcardimg: this.props.playercardselect[0].img,
        pcardhptot: this.props.playercardselect[0].fullhp,
        pcardatk: this.props.playercardselect[0].attack,
        pcarddef: this.props.playercardselect[0].defense,
        pcardbase: this.props.playercardselect[0].base,
        ecardhp: this.props.enemycardselect[0].curhp,
        ecardhptot: this.props.enemycardselect[0].fullhp,
        ecardatk: this.props.enemycardselect[0].attack,
        ecarddef: this.props.enemycardselect[0].defense,
        ecardbase: this.props.enemycardselect[0].base,
        ecardimg: this.props.enemycardselect[0].img,
        pcardatksprite: this.props.playercardselect[0].atksprite,
        pcardspritecol: this.props.playercardselect[0].spritecol,
        pcardspriterow: this.props.playercardselect[0].spriterow,
        pcardspriteframes: this.props.playercardselect[0].spriteframes,
        pcardelement: this.props.playercardselect[0].element,
        ecardelement: this.props.enemycardselect[0].element,
        ecardatksprite: this.props.enemycardselect[0].atksprite,
        ecardspritecol: this.props.enemycardselect[0].spritecol,
        ecardspriterow: this.props.enemycardselect[0].spriterow,
        ecardspriteframes: this.props.enemycardselect[0].spriteframes,
      }, () => {
        //starting combat stuff
        this._cticks = setInterval(() => {

          if (this.state.pturn) {
            //play hit sound
            this.props.global.playHit();
            // NOTES: Battle calculations
            var hit = this.state.pcardatk + Math.floor(Math.random() * this.state.pcardbase); //  ie. (10 + rnd num max of base)
            var dmg = Math.round(hit - (hit / this.state.ecarddef));// ie. (10 - (10/10))
            // dmgresult = resistance * dmg  ie. (0.5 * 9) //if they resist
            // else if weak it will be dmgresult = wk * dmg
            // res will be elmnt sm med lg
            // elmnt will be fire earth water
            var dmgtostate = this.state.ecardhp - dmg
            dmgtostate = dmgtostate < 0 ? 0 : dmgtostate;
            // console.log('Player Attacks for:');
            // console.log(dmg);
            this.pattackplay('attack');


            this.setState({
              pturn: !this.state.pturn,
              ecardhp: dmgtostate,
              eopacity: 0.25,
            }, () => { setTimeout(() => { this.setState({ eopacity: 1 }) }, 150) })
            if (dmgtostate <= 0) {
              clearInterval(this._cticks);
              //I need to know how long ecards is here!
              const ecardl = this.props.ecards.length;
              if (ecardl >= 2) {
                this.props.enemyUpdate()
              } else {
                // console.log('Win!');


                const level = this.props.navigation.getParam('level', '0');
                const firstrewards = this.props.navigation.getParam('firstrewards', 'none');
                const firstcheck = this.props.navigation.getParam('firstcheck', 'none');
                const repeatrewards = this.props.navigation.getParam('repeatrewards', 'none');

                this.props.global.toggleComplete(level)
                this.setState({ endlevel: 1 })

                if (firstcheck) {
                  //Math.floor(Math.random()*max+min);
                  var gold = Math.floor(Math.random() * (repeatrewards.maxgold - repeatrewards.mingold + 1) + repeatrewards.mingold);
                  var gems = Math.floor(Math.random() * (repeatrewards.maxgems - repeatrewards.mingems + 1) + repeatrewards.mingems);
                  this.props.global.rewardGold(gold);
                  this.props.global.rewardGems(gems);
                  this.setState({ gold: gold, gems: gems })
                } else {
                  var gold = Math.floor(Math.random() * (firstrewards.maxgold - firstrewards.mingold + 1) + firstrewards.mingold);
                  var gems = Math.floor(Math.random() * (firstrewards.maxgems - firstrewards.mingems + 1) + firstrewards.mingems);
                  this.setState({ gold: gold, gems: gems })
                  this.props.global.rewardGold(gold);
                  this.props.global.rewardGems(gems);
                  this.props.global.toggleFirstReward(level)
                }
              }

            }
          } else {
            //play hit sound
            this.props.global.playHit();

            var hit = this.state.ecardatk + Math.floor(Math.random() * this.state.ecardbase); //  ie. (10 + rnd num max of base)
            var dmg = Math.round(hit - (hit / this.state.pcarddef));// ie. (10 - (10/10))
            var dmgtostate = this.state.pcardhp - dmg
            dmgtostate = dmgtostate < 0 ? 0 : dmgtostate;
            // console.log('Enemy Attacks for:');
            // console.log(dmg);
            this.eattackplay('attack');


            this.setState({
              pturn: !this.state.pturn,
              pcardhp: dmgtostate,
              popacity: 0.25,
            }, () => { setTimeout(() => { this.setState({ popacity: 1 }) }, 150) })
            // console.log('Player Cards Length:')
            // console.log(this.props.pcards.length)
            if (dmgtostate <= 0) {
              clearInterval(this._cticks);
              const pcardl = this.props.pcards.length;
              if (pcardl >= 2) {
                this.props.playerUpdate()
              } else {
                // console.log('Lose!')
                this.setState({ endlevel: 2 })
              }
            }
          }

        }, 2000);
      });//end setstate
    }
  }
  render() {
    const end = this.state.endlevel;
    let endBanner;
    if (end == 0) {
      //do nothing
      endBanner = null;
    } else if (end == 1) {
      //win
      endBanner = <LevelSplash result={1} text='VICTORY!' navigation={this.props.navigation} gold={this.state.gold} gems={this.state.gems} />;
    } else if (end == 2) {
      //lose
      endBanner = <LevelSplash result={0} text='DEFEAT!' navigation={this.props.navigation} gold={this.state.gold} gems={this.state.gems} />;
    }
    if (this.props.playercardselect.length > 0) {
      return (
        <View style={styles.container}>

          <View style={styles.section}>
            <View style={{ opacity: this.state.eopacity, flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <View>
                <BigCard name={this.state.ecardname} element={this.state.ecardelement} stars={this.state.ecardstars} level={this.state.ecardlevel} img={this.state.ecardimg} flip={true} fullhp={this.state.ecardhptot} hp={this.state.ecardhp} attack={this.state.ecardatk} defense={this.state.ecarddef} />
                <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                  <SpriteSheet
                    ref={ref => (this.pattack = ref)}
                    source={this.props.playercardselect[0].atksprite}
                    columns={this.props.playercardselect[0].spritecol}
                    rows={this.props.playercardselect[0].spriterow}
                    // height={200} // set either, none, but not both
                    // width={200}
                    imageStyle={{ marginTop: -1 }}
                    animations={{
                      attack: this.props.playercardselect[0].spriteframes,
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.section}>
            <View style={{ opacity: this.state.popacity, flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <View>
                <BigCard name={this.state.pcardname} element={this.state.pcardelement} stars={this.state.pcardstars} level={this.state.pcardlevel} img={this.state.pcardimg} flip={false} fullhp={this.state.pcardhptot} hp={this.state.pcardhp} attack={this.state.pcardatk} defense={this.state.pcarddef} />
                <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                  <SpriteSheet
                    ref={ref => (this.eattack = ref)}
                    source={this.props.enemycardselect[0].atksprite}
                    columns={this.props.enemycardselect[0].spritecol}
                    rows={this.props.enemycardselect[0].spriterow}
                    // height={200} // set either, none, but not both
                    // width={200}
                    imageStyle={{ marginTop: -1 }}
                    animations={{
                      attack: this.props.enemycardselect[0].spriteframes,
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          {endBanner}
        </View>
      );

    } else {
      return null;
    }
  }
  componentWillUnmount() {
    clearInterval(this._cticks);
  }
  pattackplay = type => {
    this.pattack.play({
      type,
      fps: Number(26),
      loop: false,
      resetAfterFinish: false,
      onFinish: () => { }
    });
  };
  eattackplay = type => {
    this.eattack.play({
      type,
      fps: Number(26),
      loop: false,
      resetAfterFinish: false,
      onFinish: () => { }
    });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 51,
    marginBottom: 51,

  },
  section: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    flexDirection: 'column',
    alignItems: 'center',
    width: 150,
    height: 220,
    borderRadius: 10,
  },

});

export default withGlobalContext(Combat);
