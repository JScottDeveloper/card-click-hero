import React, { Component } from 'react';
import { FlatList, Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native';

import SmallCard from './SmallCard';
import GridCard from './GridCard';

export default class CardGrid extends Component {
  constructor(props) {
    super(props);
  };
  render() {
    return (
      <View style={styles.container}>
        <FlatList style={styles.flatlist} numColumns={3} data={this.props.cards}
          renderItem={({ item, i }) => {
            return (
              <TouchableOpacity onPress={() => this.props.cardSelect(item)}>
                <GridCard name={item.name} element={item.element} stars={item.stars} level={item.level} img={item.img} flip={false} hp={item.curhp} fullhp={item.fullhp} attack={item.attack} defense={item.defense} />
              </TouchableOpacity>

            );
          }}
          keyExtractor={item => item.puid}
          extraData={this.props.updated}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  innerContainer: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch'

  },
  flatlist: {

  },
  card: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    width: 110,
    height: 160,
    borderRadius: 10,
  }
});
