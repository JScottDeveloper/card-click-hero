import React from 'react';
import { StyleSheet, Platform, Image, Text, View } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

// import the different screens
import Main from './components/scenes/Main'
import Battle from './components/scenes/Battle'
import CardTeam from './components/scenes/CardTeam'
import CardDraw from './components/scenes/CardDraw'
import Fuse from './components/scenes/Fuse'
import Example from './components/scenes/Example'

// create our app's navigation stack
export default createAppContainer(createSwitchNavigator(
  {
    Main,
    Battle,
    CardTeam,
    CardDraw,
    Fuse,
    Example
  },
  {
    initialRouteName: 'Main'
  }
));
