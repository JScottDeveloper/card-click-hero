import AllCards from './AllCards';

export default Levels = [
  {
    level: 1,
    name: '1.1',
    ecards: [AllCards[0]],
    firstrewards: { maxgold: 45, mingold: 35, maxgems: 10, mingems: 10 },
    firstcheck: false,
    repeatrewards: { maxgold: 20, mingold: 12, maxgems: 0, mingems: 0 },
    idleunlocked: true,
    battlecomplete: false,
  },
  {
    level: 2,
    name: '1.2',
    ecards: [AllCards[0], AllCards[3]],
    firstrewards: { maxgold: 55, mingold: 45, maxgems: 0, mingems: 0 },
    firstcheck: false,
    repeatrewards: { maxgold: 25, mingold: 16, maxgems: 0, mingems: 0 },
    idleunlocked: false,
    expcost: 200,
    battlecomplete: false,
  },
  {
    level: 3,
    name: '1.3',
    ecards: [AllCards[0], AllCards[3], AllCards[5]],
    firstrewards: { maxgold: 65, mingold: 50, maxgems: 0, mingems: 0 },
    firstcheck: false,
    repeatrewards: { maxgold: 30, mingold: 21, maxgems: 0, mingems: 0 },
    idleunlocked: false,
    expcost: 400,
    battlecomplete: false,
  },
  {
    level: 4,
    name: '1.4',
    ecards: [AllCards[0], AllCards[2], AllCards[4]],
    firstrewards: { maxgold: 80, mingold: 65, maxgems: 0, mingems: 0 },
    firstcheck: false,
    repeatrewards: { maxgold: 45, mingold: 35, maxgems: 0, mingems: 0 },
    idleunlocked: false,
    expcost: 800,
    battlecomplete: false,
  },
  {
    level: 5,
    name: '1.5',
    ecards: [AllCards[2], AllCards[2], AllCards[0]],
    firstrewards: { maxgold: 100, mingold: 85, maxgems: 0, mingems: 0 },
    firstcheck: false,
    repeatrewards: { maxgold: 60, mingold: 50, maxgems: 0, mingems: 0 },
    idleunlocked: false,
    expcost: 1600,
    battlecomplete: false,
  },
];
