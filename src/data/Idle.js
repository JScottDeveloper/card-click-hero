
export default Idle =
  {
    idlegp: 2,
    gpmod: 0.5,
    gpcost: 24,
    gpcostmod: 2,
    gpcollect: 0,

    idlexp: 2,
    xpmod: 0.5,
    xpcost: 24,
    xpcostmod: 2,
    xpcollect: 0,
  };
