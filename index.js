
import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import { GlobalContextProvider } from './src/GlobalContext';

const Global = () => {
    return (
        <GlobalContextProvider>
          <App />
        </GlobalContextProvider>
    );
}

AppRegistry.registerComponent(appName, () => Global);
